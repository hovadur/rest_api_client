@TestOn('vm')
@Timeout(Duration(seconds: 10))
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:rest_api_client/rest_api_client.dart';
import 'package:rest_api_client/src/multipart_request.dart';
import 'package:test/test.dart';

void main() {
  late Uri apiUri;
  late ApiClient apiClient;

  setUpAll(() async {
    final channel = spawnHybridUri('helpers/http_server.dart', stayAlive: true);
    final String hostPort = await channel.stream.first;
    apiUri = Uri.http(hostPort, '/');
    apiClient = ApiClient(apiUri,
        onBeforeRequest: (request) => request.change(
            headers: Map.from(request.headers)
              ..addAll({'X-Requested-With': 'XMLHttpRequest'})),
        onAfterResponse: (response) =>
            response.change(headers: {'X-Added-By-Callback': 'value'}));
  });

  test('upload file vm test', () async {
    final file = File('test/helpers/test_image.jpg');
    int expectedSize = await file.length();
    int onProgressCalledTimes = 0;
    bool onCompleteCalled = false;
    final response = await apiClient.send(UploadRequest(
        resourcePath: 'files',
        file: file,
        onProgress: (sentBytes, totalBytes) {
          ++onProgressCalledTimes;
        },
        onComplete: () {
          onCompleteCalled = true;
        }));
    int expectedOnProcessCalls = (expectedSize / (64 * 1024)).ceil();
    expect(response.statusCode, HttpStatus.ok);
    expect(response.body, {
      'type': 'image/jpeg',
      'name': 'test_image.jpg',
      'bytesCount': expectedSize
    });
    expect(onProgressCalledTimes, expectedOnProcessCalls);
    expect(onCompleteCalled, true);
  }, testOn: 'vm');

  test('multipart request vm test', () async {
    String filePath = 'test/helpers/test_image.jpg';
    expect(
        FileSystemEntity.typeSync(filePath) == FileSystemEntityType.file, true);
    final file = File(filePath);
    final fileContent = file.readAsBytesSync();
    int fileSize = fileContent.length;
    expect(fileContent.length, await file.length());
    MultipartRequest multipartRequest = MultipartRequest(
        resourcePath: 'form-multipart',
        headers: {'x-requested-with': 'XMLHttpRequest'});
    multipartRequest.files.add(http.MultipartFile.fromBytes('file', fileContent,
        filename: 'test_image.jpg', contentType: MediaType('image', 'jpeg')));
    final response = await apiClient.send(multipartRequest);
    expect(response.statusCode, HttpStatus.ok);
    int contentLength = int.parse(response.body['Content-Length']);
    String contentType = response.body['Content-Type'];
    expect(contentLength > fileSize, true);
    expect(contentType.contains('multipart/form-data'), true);
  }, testOn: 'vm');
}
