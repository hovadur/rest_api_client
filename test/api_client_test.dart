import 'dart:convert';

import 'package:data_model/data_model.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:rest_api_client/rest_api_client.dart';
import 'package:test/test.dart';

main() {
  late Uri apiUri;
  late ApiClient apiClient;
  final requestParams = {'param1': 'value1', 'param2': 'value2'};

  setUpAll(() async {
    final channel = spawnHybridUri('helpers/http_server.dart', stayAlive: true);
    final String hostPort = await channel.stream.first;
    apiUri = Uri.http(hostPort, '/');
    apiClient = ApiClient(apiUri,
        onBeforeRequest: (request) => request.change(
            headers: Map.from(request.headers)
              ..addAll({'X-Requested-With': 'XMLHttpRequest'})),
        onAfterResponse: (response) => response.change(
            headers: Map.from(response.headers)
              ..addAll({'X-Added-By-Callback': 'value'})));
  });

  test('get request', () async {
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.get,
        resourcePath: '/resource',
        queryParameters: requestParams));
    expect(response.statusCode, HttpStatus.ok);
    expect(response.headers['X-Added-By-Callback'], 'value');
    expect(response.body, {
      'method': 'GET',
      'uri': apiUri
          .replace(path: '/resource', queryParameters: requestParams)
          .toString()
    });
  });

  test('post request', () async {
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.post,
        resourcePath: '/resource',
        body: requestParams));
    expect(response.statusCode, HttpStatus.ok);
    expect(response.headers['X-Added-By-Callback'], 'value');
    expect(response.body, {
      'method': 'POST',
      'uri': apiUri
          .replace(
            path: '/resource',
          )
          .toString(),
      'body': requestParams
    });
  });

  test('put request', () async {
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.put,
        resourcePath: '/resource',
        body: requestParams));
    expect(response.statusCode, HttpStatus.ok);
    expect(response.headers['X-Added-By-Callback'], 'value');
    expect(response.body, {
      'method': 'PUT',
      'uri': apiUri
          .replace(
            path: '/resource',
          )
          .toString(),
      'body': requestParams
    });
  });

  test('patch request', () async {
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.patch,
        resourcePath: '/resource',
        body: requestParams));
    expect(response.statusCode, HttpStatus.ok);
    expect(response.headers['X-Added-By-Callback'], 'value');
    expect(response.body, {
      'method': 'PATCH',
      'uri': apiUri
          .replace(
            path: '/resource',
          )
          .toString(),
      'body': requestParams
    });
  });

  test('delete request', () async {
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.delete,
        resourcePath: '/resource',
        queryParameters: requestParams));
    expect(response.statusCode, HttpStatus.ok);
    expect(response.headers['X-Added-By-Callback'], 'value');
    expect(response.body, {
      'method': 'DELETE',
      'uri': apiUri
          .replace(path: '/resource', queryParameters: requestParams)
          .toString()
    });
  });

  test('get request with unauthorized response', () async {
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.get,
        resourcePath: '/unauthorized',
        queryParameters: requestParams));
    expect(response.statusCode, HttpStatus.unauthorized);
    expect(response.headers['X-Added-By-Callback'], 'value');
    expect(response.reasonPhrase, '${HttpStatus.unauthorized}-Unauthorized');
  });

  test('get request with internal server error response', () async {
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.get,
        resourcePath: '/servererror',
        queryParameters: requestParams));
    expect(response.statusCode, HttpStatus.internalServerError);
    expect(response.headers['X-Added-By-Callback'], 'value');
    expect(response.reasonPhrase,
        '${HttpStatus.internalServerError}-Internal Server Error');
  });

  test('post request with unauthorized response', () async {
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.post,
        resourcePath: '/unauthorized',
        body: requestParams));
    expect(response.statusCode, HttpStatus.unauthorized);
    expect(response.headers['X-Added-By-Callback'], 'value');
    expect(response.reasonPhrase, '${HttpStatus.unauthorized}-Unauthorized');
  });

  test('put request with unauthorized response', () async {
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.put,
        resourcePath: '/unauthorized',
        body: requestParams));
    expect(response.statusCode, HttpStatus.unauthorized);
    expect(response.headers['X-Added-By-Callback'], 'value');
    expect(response.reasonPhrase, '${HttpStatus.unauthorized}-Unauthorized');
  });

  test('patch request with unauthorized response', () async {
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.patch,
        resourcePath: '/unauthorized',
        body: requestParams));
    expect(response.statusCode, HttpStatus.unauthorized);
    expect(response.headers['X-Added-By-Callback'], 'value');
    expect(response.reasonPhrase, '${HttpStatus.unauthorized}-Unauthorized');
  });

  test('delete request with unauthorized response', () async {
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.get,
        resourcePath: '/unauthorized',
        queryParameters: requestParams));
    expect(response.statusCode, HttpStatus.unauthorized);
    expect(response.headers['X-Added-By-Callback'], 'value');
    expect(response.reasonPhrase, '${HttpStatus.unauthorized}-Unauthorized');
  });

  test('toEncodable()', () async {
    final encodable = EncodableObject();
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.post,
        resourcePath: '/echo-resource',
        body: {'test': encodable}));
    expect(response.statusCode, HttpStatus.ok);
    expect(response.headers['X-Added-By-Callback'], 'value');
    expect(response.body, {
      'test': {
        'id': 1234567890,
        'date': encodable.date.toUtc().toIso8601String()
      }
    });
  });

  test('binary content in response', () async {
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.get,
        resourcePath: '/resource',
        headers: {'Accept': ContentType.binary.toString()},
        queryParameters: requestParams));
    expect(response.statusCode, HttpStatus.ok);
    expect(response.headers['content-type'], ContentType.binary.toString());
    expect(response.body, TypeMatcher<List<int>>());
    expect(
        response.body,
        json.encode({
          'method': 'GET',
          'uri': apiUri
              .replace(path: '/resource', queryParameters: requestParams)
              .toString()
        }).codeUnits);
  });

  test('unsupported response mime-type', () {
    expect(
        () => apiClient.send(ApiRequest(
            method: HttpMethod.get, resourcePath: 'unsupported-mime')),
        throwsA(TypeMatcher<UnsupportedError>()));
  });

  // TODO: test('skip request', () {
  //   final apiClient = ApiClient(apiUri, onBeforeRequest: (request) => null);
  //   expect(apiClient.send(ApiRequest()), completion(null));
  // });

  test('request is in progress', () {
    final request = apiClient.send(ApiRequest(
        method: HttpMethod.get,
        resourcePath: '/resource',
        queryParameters: requestParams));
    expect(apiClient.requestInProgress, true);
    expect(request,
        completion(predicate((value) => apiClient.requestInProgress == false)));
  });

  test('request progress in case of exception', () {
    final client = ApiClient(Uri.parse('http://unexistent.uri'));
    final request =
        client.send(ApiRequest(method: HttpMethod.get, resourcePath: '/'));
    expect(client.requestInProgress, true);
    expect(request,
        throwsA(predicate((value) => client.requestInProgress == false)));
  });
  test('using alternative http client', () async {
    final httpClient =
        MockClient((request) async => Response(json.encode('response'), 200));
    final client =
        ApiClient(Uri.parse('http://api.test.uri'), httpClient: httpClient);
    final response = await client
        .send(ApiRequest(method: HttpMethod.get, resourcePath: '/'));
    expect(response.body, 'response');
  });
}

class EncodableObjectId extends ObjectId {
  EncodableObjectId._(id) : super(id);

  factory EncodableObjectId(id) {
    return EncodableObjectId._(id);
  }
}

class EncodableObject implements JsonEncodable {
  final id = EncodableObjectId(1234567890);
  final date = DateTime.now();

  Map<String, dynamic> get json => {'id': id, 'date': date};
}
