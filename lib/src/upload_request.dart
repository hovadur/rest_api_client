import 'api_request.dart';
import 'uploader.dart';

/// Request to upload file
class UploadRequest extends ApiRequest {
  /// On progress callback
  ///
  /// The callback is called after each chunck of data is sent to the server
  ///
  /// Callback takes two [int] parameters:
  ///
  /// - `bytesSent`
  /// - `bytesTotal`
  final OnProgress? onProgress;

  /// On data send complete callback
  ///
  /// The callback is called after all data is sent to the server
  final OnComplete? onComplete;

  /// On error callback
  final Function? onError;

  /// Creates UploadRequest
  ///
  /// All parameters are equivalent to the corresponding parameters of [ApiRequest] class
  ///
  /// `file` - must be a [File] (from `dart:io` or `dart:html`)
  UploadRequest(
      {String resourcePath = '',
      Map<String, dynamic>? queryParameters,
      Map<String, String>? headers,
      /*File*/ file,
      this.onProgress,
      this.onComplete,
      this.onError})
      : super(
            method: HttpMethod.post,
            resourcePath: resourcePath,
            queryParameters: queryParameters,
            headers: headers,
            body: file);

  @override
  UploadRequest change(
          {Map<String, String>? queryParameters,
          Map<String, String>? headers}) =>
      UploadRequest(
          resourcePath: this.resourcePath,
          queryParameters:
              queryParameters != null ? queryParameters : this.queryParameters,
          headers: headers != null ? headers : this.headers,
          file: this.body,
          onProgress: this.onProgress,
          onComplete: this.onComplete,
          onError: this.onError);
}
