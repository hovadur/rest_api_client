/// Api-server response
class ApiResponse {
  /// Http-status code
  final int statusCode;

  late Map<String, String> _headers;

  /// Message from server
  final String reasonPhrase;

  /// Response body
  final body;

  ApiResponse(
      {required this.statusCode,
      required this.reasonPhrase,
      Map<String, String> headers = const {},
      this.body}) {
    _headers = Map.unmodifiable(headers);
  }

  /// Response headers
  Map<String, String> get headers => _headers;

  /// Changes response
  ///
  /// Returns new [ApiResponse] with changed parameters
  /// If nothing to change returns a copy of the response
  ApiResponse change(
          {int? statusCode,
          String? reasonPhrase,
          Map<String, String>? headers,
          body}) =>
      ApiResponse(
          statusCode: statusCode == null ? this.statusCode : statusCode,
          reasonPhrase: reasonPhrase == null ? this.reasonPhrase : reasonPhrase,
          headers: headers == null ? this.headers : headers,
          body: body == null ? this.body : body);
}
