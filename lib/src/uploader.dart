import 'dart:async';

import 'package:http/http.dart' as http;

// ignore: uri_does_not_exist
import 'uploader_stub.dart'
    // ignore: uri_does_not_exist
    if (dart.library.html) 'browser_uploader.dart'
    // ignore: uri_does_not_exist
    if (dart.library.io) 'vm_uploader.dart';

/// On progress callback type
typedef OnProgress = Function(int sentBytes, int totalBytes);

/// On complete callback type
typedef OnComplete = Function();

/// Uploader interface
abstract class Uploader {
  factory Uploader() => createUploader();
  Future<http.Response> call(file, Uri uri,
      {Map<String, String> headers,
      OnProgress? onProgress,
      OnComplete? onComplete,
      Function? onError});
}
