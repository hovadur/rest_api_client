import 'package:http/http.dart';

import 'api_request.dart';

/// A `multipart/form-data` request. Such a request has both string [fields],
/// which function as normal form fields, and (potentially streamed) binary
/// [files].
class MultipartRequest extends ApiRequest {
  /// The form fields to send for this request.
  final Map<String, String> fields;

  /// The private version of [files].
  final List<MultipartFile> _files;

  /// Creates a new [MultipartRequest].
  MultipartRequest({
    HttpMethod method = HttpMethod.post,
    String resourcePath = '',
    Map<String, dynamic>? queryParameters,
    Map<String, String>? headers,
  })  : fields = {},
        _files = <MultipartFile>[],
        super(
            method: method,
            resourcePath: resourcePath,
            queryParameters: queryParameters,
            headers: headers);

  /// The list of files to upload for this request.
  List<MultipartFile> get files => _files;

  /// Returns string representation for [HttpMethod]
  String get methodName {
    String result;
    switch (method) {
      case HttpMethod.get:
        result = 'GET';
        break;
      case HttpMethod.post:
        result = 'POST';
        break;
      case HttpMethod.put:
        result = 'PUT';
        break;
      case HttpMethod.patch:
        result = 'PATCH';
        break;
      case HttpMethod.delete:
        result = 'DELETE';
        break;
    }
    return result;
  }

  @override
  MultipartRequest change(
      {Map<String, String>? queryParameters, Map<String, String>? headers}) {
    final multipartRequest = MultipartRequest(
      method: this.method,
      resourcePath: this.resourcePath,
      queryParameters:
          queryParameters != null ? queryParameters : this.queryParameters,
      headers: headers != null ? headers : this.headers,
    );
    multipartRequest.fields.addAll(fields);
    multipartRequest.files.addAll(_files);
    return multipartRequest;
  }
}
